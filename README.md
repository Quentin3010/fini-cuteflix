# CUTEFLIX

Quentin BERNARD & Robin LEFEBVRE & Thomas BRICHE

## Description du projet

Le projet “Cuteflix” est un site web parodique de Netflix, entièrement développé en JavaScript. Ce projet a été réalisé dans le cadre de notre fin de semestre en cours de JavaScript. 

L’objectif du projet était de créer un site web créatif qui imite le design et les fonctionnalités de Netflix. Grâce à nos connaissances en JavaScript, nous avons pu mettre en place une interface interactive et dynamique, permettant aux utilisateurs de naviguer à travers différentes catégories de films et de séries, de visualiser des informations sur chaque titre.

Vidéo de présentation : lien

## Fonctionnalités

- Page d'accueil :
	- Affichage d'une liste de séries avec leur image et leur titre.
	- Au survol d'une série, une description de la série est affichée.
	- Lorsque l'utilisateur clique sur une série, une page détaillée s'ouvre.
- Page détaillée d'une série :
	- Affichage du titre, de l'image et des informations de la série.
	- Liste des 5 derniers épisodes avec des miniatures.
	- Possibilité de cliquer sur un bouton spoiler pour cacher les miniatures et éviter les spoilers.
- Page de l'équipe :
	- Présentation des membres de l'équipe de création du site.
	- Affichage des informations et de l'implication de chaque membre dans le projet.
- Single Page Application :
	- Le site est conçu en tant qu'application monopage (SPA), offrant une navigation fluide sans rechargement de page.

## Mon rôle

J'ai été chargé de plusieurs tâches essentielles pour le projet du site web parodique de Netflix. Mon rôle comprenait la création de la fenêtre de détails des séries, la mise en place de la liste des 5 derniers épisodes, la conception des pages de l'équipe et de la page d'accueil, ainsi que la contribution au CSS global du site. J'ai veillé à ce que les informations soient présentées de manière claire et attrayante, en mettant l'accent sur une expérience utilisateur immersive. C'était un projet où j'ai pu mettre en pratique ma créativité et mes compétences techniques pour offrir une expérience divertissante aux utilisateurs.

