import Page from './Page';
import SerieThumbnail from '../components/SerieThumbnail';
import SerieDetail from './SerieDetail';
import Router from '../Router';

export default class SerieList extends Page {
	#series;
	saisie;

	constructor(series) {
		super('SerieList');
		this.series = series;
	}

	render() {
		const html = /*html*/ `
		<form class="searchBar" >
		<h2>Recherche</h2>

			<input type="text" name="saisie">
			<input type="submit" value="Submit">
		</form>
		<div class=tri>
		<label for="tri">Trier par:</label>
		<select name="tri" id="tri">
    		<option value="Nom">Nom</option>
    		<option value="Date">Date</option>
    		<option value="Note">Note</option>
		</select>
		<button id="reverseButton">↕️</button>
		</div>
		<div class="seriesContainer"></div>
		`;
		return html;
	}

	mount(element) {
		super.mount(element);
		const form = document.querySelector('.searchBar');
		const tri = document.getElementById('tri');
		const reverseButton = document.getElementById('reverseButton');
		this.saisie = form.querySelector('input[name=saisie]');

		const seriesContainer = document.querySelector(
			'.seriesContainer',
			this.element
		);

		this.randomSeriesChoice(seriesContainer);

		form.addEventListener('submit', event => {
			event.preventDefault();
			this.saisie = form.querySelector('input[name=saisie]');
			this.submit(event);
		});

		tri.addEventListener('change', event => {
			event.preventDefault();
			this.change(event.target.value);
		});

		reverseButton.addEventListener('click', event => {
			event.preventDefault();
			this.series = this.#series.reverse();
		});
	}

	submit() {
		const seriesContainer = document.querySelector('.seriesContainer');
		if (this.saisie.value === '') {
			this.randomSeriesChoice(seriesContainer);
			return;
		}
		seriesContainer?.classList.add('is-loading');
		fetch(`https://api.tvmaze.com/search/shows?q=${this.saisie.value}`)
			.then(response => response.json())
			.then(data => {
				this.series = data.map(serieFromJson => serieFromJson.show);
			})
			.then(() => seriesContainer.classList.remove('is-loading'));
		return;
	}

	change(value) {
		switch (value) {
			case 'Date':
				let sortListDate = this.#series;
				sortListDate.sort(function (a, b) {
					const date1 = new Date(a.premiered);
					const date2 = new Date(b.premiered);
					return (
						(a.premiered === null) - (b.premiered === null) || date2 - date1
					);
				});
				this.series = sortListDate;
				break;
			case 'Nom':
				let sortListName = this.#series;
				sortListName.sort(function (a, b) {
					return a.name.localeCompare(b.name);
				});
				this.series = sortListName;
				break;
			case 'Note':
				let sortListNote = this.#series;
				sortListNote.sort(function (a, b) {
					return (
						(a.rating === undefined) - (b.rating === undefined) ||
						(a.rating.average === undefined) -
							(b.rating.average === undefined) ||
						b.rating.average - a.rating.average
					);
				});
				this.series = sortListNote;
				break;
		}
		return;
	}

	randomSeriesChoice(seriesContainer) {
		seriesContainer?.classList.add('is-loading');
		fetch('https://api.tvmaze.com/shows')
			.then(response => response.json())
			.then(data => {
				const ListeSeries = data;
				let RandomSeries = [];
				for (let i = 0; i < 10; i++) {
					let randomNumber = Math.floor(Math.random() * ListeSeries.length);
					while (RandomSeries.includes(ListeSeries[randomNumber])) {
						randomNumber = Math.floor(Math.random() * ListeSeries.length);
					}
					RandomSeries[i] = ListeSeries[randomNumber];
				}

				this.#series = RandomSeries;
				this.change('Nom');
			})
			.then(() => seriesContainer?.classList.remove('is-loading'));
		return;
	}

	set series(value) {
		this.#series = value;
		const seriesContainer = document.querySelector('.seriesContainer');
		this.children = this.#series.map(serie => new SerieThumbnail(serie));
		if (this.children != undefined && seriesContainer != undefined) {
			seriesContainer.innerHTML = '';
			for (let i = 0; i < this.children?.length; i++) {
				seriesContainer.innerHTML += this.children[i].render();
			}
		}
		SerieList.addEventForAllElements();
	}
	getSeries() {
		return this.#series;
	}

	static addEventForAllElements() {
		const seriesThumbnail = document.querySelectorAll('.serieThumbnail');
		for (const element of seriesThumbnail) {
			//GESTION DE L'AFFICHAGE DE LA "PAGE" DETAIL
			element.addEventListener('click', function (event) {
				Router.navigate('/serie-' + event.currentTarget.id);
			});
		}
	}
}
