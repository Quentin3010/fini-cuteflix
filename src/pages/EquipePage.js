import Page from './Page';
import SerieThumbnail from '../components/SerieThumbnail';
import Router from '../Router';

export default class EquipePage extends Page {
	render() {
		return /*html*/ `
        <article >
            <p>
            <div class="equipe">
                <div class="cadre">
                    <h2>Robin Lefebvre</h2>
                    <img src="images/robin_img.png">
                    <h3>Pourcentage d'investissement : 100%</h3>
                    <div class="progressbar" id="robin"></div>
                    <div class="seriepref" id="kaguya">
                <h2>Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen</h2>
                <img src="images/kaguya.jpg">
            </div>
			</div>
            <div class="cadre">
                <h2>Quentin Bernard</h2>
                <img src="images/quentin_img.png">
                <h3>Pourcentage d'investissement : 100%</h3>
                <div class="progressbar" id="quentin"></div>
                <div class="seriepref" id="konosuba">
                <h2>Kono Subarashii Sekai ni Shukufuku o!</h2>
                <img src="images/konosuba.jpg">
            </div>
            </div>
            <div class="cadre">
                <h2>Thomas Briche</h2>
                <img src="images/thomas_img.png">
                <h3>Pourcentage d'investissement : 60%</h3>
                <div class="progressbar" id="thomas"></div>
                <div class="seriepref" id="noGame">
                    <h2>No Game No Life</h2>
                    <img src="images/ngnl.jpg">
                </div>
            </div>    
            </div>
            </p>
        </article>`;
	}
	mount(element) {
		super.mount(element);
		const konosuba = document.getElementById('konosuba');
		const kaguya = document.getElementById('kaguya');
		const noGame = document.getElementById('noGame');
		konosuba.addEventListener('click', event => {
			event.preventDefault();
			Router.navigate('/serie-10772');
		});
		kaguya.addEventListener('click', event => {
			event.preventDefault();
			Router.navigate('/serie-40143');
		});
		noGame.addEventListener('click', event => {
			event.preventDefault();
			Router.navigate('/serie-5470');
		});
	}
}
