import Page from './Page.js';
import Episode from '../components/Episode';
import SerieDetailThumbnail from '../components/SerieDetailThumbnail';
import Router from '../Router';

export default class SerieDetail extends Page {
	id;

	constructor(id) {
		super('SerisDetail');
		this.id = id;
	}

	render() {
		const html =
			/*html*/
			`<div class="serieDetailContent">
				<div class="serieDetailThumbnail"></div>
			</div>`;
		return html;
	}

	mount(element) {
		super.mount(element);
		this.getInfoBase();
	}
	getInfoBase() {
		const element = document.querySelector('.serieDetailThumbnail');
		element.classList.add('is-loading');
		fetch(`https://api.tvmaze.com/shows/${this.id}`)
			.then(response => response.text())
			.then(responseText => {
				const serieDetail = new SerieDetailThumbnail(JSON.parse(responseText));
				element.innerHTML = serieDetail.render();
			})
			.then(() => {
				element.innerHTML += '<div class="listeEpisodes">';
				this.getListEpisode();
			})
			.then(() => element.classList.remove('is-loading'));
		return element;
	}

	getListEpisode() {
		const element = document.querySelector('.listeEpisodes');
		fetch(`https://api.tvmaze.com/shows/${this.id}/episodes`)
			.then(response => response.text())
			.then(responseText => {
				const listeEpisodes = JSON.parse(responseText);
				if (listeEpisodes.length != 0) {
					let size = 5;
					if (listeEpisodes.length < 5) size = listeEpisodes.length;
					for (
						let i = listeEpisodes.length - size;
						i < listeEpisodes.length;
						i++
					) {
						const episodeDetail = new Episode(listeEpisodes[i]);
						element.innerHTML += episodeDetail.render();
					}
				}
			})
			.then(() => {
				element.innerHTML += '</div>';
				const e = document.querySelector('.serieDetailThumbnail');
			})
			.then(() => this.eventSpoilButton());
		return;
	}

	eventSpoilButton() {
		const buttonsSpoil = document.querySelectorAll('button');
		buttonsSpoil.forEach(button => {
			button.addEventListener('click', event => {
				event.preventDefault();
				const descToChange = document.getElementById(
					'Desc' + button.getAttribute('id')
				);
				if (descToChange.getAttribute('style') === 'visibility:hidden') {
					descToChange.setAttribute('style', 'visibility:visible');
				} else {
					descToChange.setAttribute('style', 'visibility:hidden');
				}
			});
		});
	}
}
