import Component from '../components/Component';
import SerisList from './SerieList.js';

export default class Page extends Component {
	element;

	constructor(className, children) {
		super('section', { name: 'class', value: className }, children);
	}
	mount(element) {
		this.element = element;
	}
}
