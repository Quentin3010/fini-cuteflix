import Router from './Router';
import SerieList from './pages/SerieList';
import EquipePage from './pages/EquipePage.js';
import $ from 'jquery';
import SerieDetail from './pages/SerieDetail.js';

$('.logo').append('<small>~ U w U ~');

const serieList = new SerieList([]),
	equipePage = new EquipePage(),
	serieDetail = new SerieDetail();
const regex = /^\/serie-[\d]+$/gm;

Router.routes = [
	{ path: '/', page: serieList, title: 'liste' },
	{ path: '/equipe', page: equipePage, title: 'équipe' },
	{ path: regex, page: serieDetail, title: 'détail' },
];

Router.titleElement = $('.pageTitle');
Router.contentElement = $('.pageContent');
Router.menuElement = $('.mainMenu');
window.onpopstate = () => {
	Router.navigate(document.location.pathname, false);
};
Router.navigate(document.location.pathname);

const element = document.querySelector('.logo');
element.addEventListener('click', () => {
	const music = new Audio('audio/pouet.mp3');
	music.play();
});
