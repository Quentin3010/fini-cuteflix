import Component from './Component.js';
import Img from './Img.js';

export default class Episode extends Component {
	episode;
	constructor(episode) {
		super('Episode', { name: 'class', value: 'Episode' });
		this.episode = episode;
	}
	render() {
		let html = `<article class="Episode" >`;

		//NUMERO EPISODE
		html += `<h1>Episode ${this.episode.number}</h1>\n`;

		//TITRE
		if (this.episode.name == undefined) html += `No name :c </h2>\n`;
		else {
			if (this.episode.name.length <= 42)
				html += `<h2>${this.episode.name}</h2>\n`;
			else {
				const name = this.episode.name.substring(0, 42);
				html += `<h2>${name}...</p></h2>\n`;
			}
		}

		//IMAGE
		if (
			this.episode.image == undefined ||
			this.episode.image.medium == undefined
		)
			html += `<img src="./images/not-found-video.jpg"`;
		else
			html += `<a href="${this.episode.url}"> <img class="imageSerie" src="${this.episode.image.medium}"> </a>`;

		//DATE
		if (this.episode.airdate == undefined) html += `<span>??-??-????</span>`;
		else html += `<span>${this.episode.airdate}</br></span>`;

		//DESCRIPTION
		if (this.episode.summary == undefined)
			html += `<span class="description"> <p> Excusez-nous ! Il n'y a pas de résumé pour cette épisode ! :c </p> </span>\n`;
		else {
			html += `<button class="spoilButton" id="${this.episode.id}" >Spoil !</button>`;
			if (this.episode.summary.length <= 128)
				html += `<span class="description" id="Desc${this.episode.id}" style="visibility:hidden">${this.episode.summary}</span>\n`;
			else {
				const summary = this.episode.summary.substring(0, 128);
				html += `<span class="description" id="Desc${this.episode.id}" style="visibility:hidden">${summary}...</p></span>\n`;
			}
		}
		return html;
	}
}
