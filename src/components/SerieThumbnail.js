import Component from './Component.js';
import Img from './Img.js';

export default class SerieThumbnail extends Component {
	serie;

	constructor(serie) {
		super('Série', { name: 'class', value: 'SerieThumbnail' });
		this.serie = serie;
	}
	render() {
		let html = `<article id="${this.serie.id}" class="serieThumbnail">
		<h1>${this.serie.name}</h1>
		<div class="serieAll">
		<div class="serieImage">
			<img src="`;

		if (this.serie.image == undefined || this.serie.image.medium == undefined)
			html += './images/not-found.jpg">';
		else html += `${this.serie.image?.medium}">`;

		html += `</div> <div class="serieInformation">`;
		if (this.serie.premiered == undefined) {
			html += `<span class="hide"> ??-??-????<br><br></span>`;
		} else {
			html += `<span class="hide"> ${this.serie.premiered}</span>`;
		}

		if (this.serie.summary == undefined) {
			html += `<span class="hide">Excusez-nous ! Il n'y a pas de résumé pour cette série ! :c</span>`;
		} else {
			if (this.serie.summary.length <= 250)
				html += `<span class="hide">${this.serie.summary}</span>`;
			else {
				let summary = this.serie.summary.substring(0, 250);
				html += `<span class="hide">${summary}...</p></span>`;
			}
		}

		if (
			this.serie.rating.average == undefined ||
			this.serie.rating == undefined
		) {
			html += `<span class="hide">??/10 </span>`;
		} else {
			html += `<span class="hide">${this.serie.rating.average}/10 </span>`;
		}
		return html + `</div> </article>`;
	}
}
