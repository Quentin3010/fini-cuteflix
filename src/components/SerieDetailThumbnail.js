import Component from './Component.js';
import Img from './Img.js';

export default class SerieDetailThumbnail extends Component {
	serie;

	constructor(serie) {
		super('Série', { name: 'class', value: 'SerieDetailThumbnail' });
		this.serie = serie;
	}

	render() {
		let html = `<article id="${this.serie.id}" class="serieDetail">
		<div class="detailSerieAll">
		<div class="serieLeft">\n`;

		//Image
		if (this.serie.image == undefined || this.serie.image.medium == undefined)
			html += '<img src="./images/not-found.jpg">\n';
		else html += `<img src="${this.serie.image?.medium}">\n`;

		html += '</br>';

		//DATE
		if (this.serie.premiered == undefined)
			html += `<span class="date"> ??-??-????</span>\n`;
		else html += `<span class="date"> ${this.serie.premiered}</span>\n`;

		html += '</br>';

		//NOTE
		if (
			this.serie.rating.average == undefined ||
			this.serie.rating == undefined
		) {
			html += `<span>??/10 </span>\n`;
		} else {
			html += `<span>${this.serie.rating.average}/10 </span>\n`;
		}

		html += `</br></div> \n<div class="serieRight">\n`;

		html += `<h1>${this.serie.name}</h1>`;

		//URL
		if (this.serie.url == undefined)
			html += `<span class="url">Pas de site pour la série ! :c</span>\n`;
		else
			html += `<a href="${this.serie.url}"> <span class="url">${this.serie.url} </span> </a>\n`;

		html += '</br>';

		//RESUME
		if (this.serie.summary == undefined)
			html += `<span class="description">Excusez-nous ! Il n'y a pas de résumé pour cette série ! :c</span>\n`;
		else {
			if (this.serie.summary.length <= 500)
				html += `<span class="description">${this.serie.summary}</span>\n`;
			else {
				const summary = this.serie.summary.substring(0, 500);
				html += `<span class="description">${summary}...</p></br></br></span>\n`;
			}
		}

		return html + `</div></article>`;
	}
}
